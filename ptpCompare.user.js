// ==UserScript==
// @name         PTP Torrent Compare
// @author       Korrug
// @description  Comparing encode settings
// @namespace    PTPTorrentCompare
// @include      https://passthepopcorn.me/torrents.php?id=*
// @version      0.2
// @downloadURL  https://gitlab.com/korrug/PTP/raw/master/ptpCompare.user.js
// @updateURL    https://gitlab.com/korrug/PTP/raw/master/ptpCompare.user.js
// @grant        none
// ==/UserScript==

var readyStateCheckInterval = setInterval(() => {
    if (document.readyState === "interactive" || document.readyState === "complete") {
        clearInterval(readyStateCheckInterval);
        main();
    }
}, 100);

function main() {
    // Need to modify this so that when you checkbox a torrent, the torrentid is saved to a var
    // should be able to pull this via the url
    var searchParams = new URLSearchParams( window.document.URL );
    var initiallyOpenedTorrentId = searchParams.get( "torrentid" );
    if ( initiallyOpenedTorrentId !== null ) {
        var group = jQuery( "#torrent_" + initiallyOpenedTorrentId );
        if ( group.length > 0 ) {
            validateGroup( group );
        }
    }

    // Mediainfo is loaded async, so use a mutation observer to detect when it's ready
    var observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        if (mutation.removedNodes[0].textContent === "Loading...") {
            validateGroup(jQuery("#torrent_" + mutation.target.id.split('_').pop()));
        }
      });
    });

    // Validate new groups when opened and ready
    jQuery('tr.torrent_info_row[style*="display: none"], tr.torrent_info_row:hidden').each((key, group) => {
        observer.observe(jQuery("#nfo_text_" + group.id.split('_').pop())[0], {childList: true});
    });
}

/**
 * Runs the suit of tests against a group.
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return null
 */
function validateGroup(group) {
    setup(group);

	// Do our work
	checkBoxes(group)
	getEncodeSettings(group);
	compare(group);

}

/**
 * Runs preflight setup
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return null
 */

function setup(group) {
    // Create someplace centralized to store EncSet for the group
    group.data("encset", []);

    // Create place to display the results of the script
    group.find('.movie-page__torrent__panel').find('.bbcode-table-guard').last().prepend("<div class='torrent_info_row_results2'></div>");
}


//create checkboxes beside eligible encodes (not a source)
function checkBoxes(group) {
    var shortInfo = getShortInfoText(group);
	var mediaInfoText = group.find('blockquote.spoiler').text();
	if (_contains(shortInfo, 'BD25') || _contains(shortInfo, 'BD50') || _contains(shortInfo, 'BD66') || _contains(shortInfo, 'BD100') || _contains(shortInfo, 'DVD5') || _contains(shortInfo, 'DVD9') || _contains(shortInfo, 'Remux')) {
		return;
	}else if(_doesntContain(mediaInfoText, 'Encoding settings')){
		return;
	}else{
        //create a place to display the checkbox
		addEncSet(group, "These things should have checkboxes");
	}
}

// getEncodeSettings should be run against the PL with a checkbox
function getEncodeSettings(group) {
	var shortInfo = getShortInfoText(group);
	var mediaInfoText = group.find('blockquote.spoiler').text();
	// If it's a BD, DVD or remux then we're done here
    if (_contains(shortInfo, 'BD25') || _contains(shortInfo, 'BD50') || _contains(shortInfo, 'BD66') || _contains(shortInfo, 'BD100') || _contains(shortInfo, 'DVD5') || _contains(shortInfo, 'DVD9') || _contains(shortInfo, 'Remux')) {
        return;
    }
	//The section of the MI we want is 'Encoding setting :'
	/* we need to store the encode settings portion of the mediainfo
	*  the data should then be passed to something like diffchecker.com
	*/
    //var rx = /^Encoding settings                        \: (.*)$/gm
    var rx = /^Encoding settings.*\: (.*)$/gm
    console.log(rx);
    var arr = rx.exec(mediaInfoText);
    if (arr){
        addEncSet(group, arr[1]);
    }
}

//here we should populate the data into something like diffchecker.com
function compare(group) {
}


/**
 * Get the "short info" description for a torrent
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return {String} e.g. "DVD9 / VOB IFO / DVD / PAL"
 */
function getShortInfoText(group) {
    return group.prev().find('.torrent-info-link').text();
}

/**
 * Add an error to the error array and update the visible error list
 * @param {object} group - The jquery "group" element to run the analysis on.
 */
function addEncSet(group, newEncSet) {
    if (newEncSet && _doesntContain(group.data("encset"), newEncSet)) {
        group.data("encset").push(newEncSet);
        displayEncSet(group);
    }
}

function displayEncSet(group, finalRun) {
    var results = '';

    if (group.data("encset").length > 0) {
        var errorMessage = "Encoding Settings";
        group.data("encset").forEach(value => {errorMessage += "<br />&#8227; " + value;});

        results = "<div class='torrent_info_row_errors2'>" +
            errorMessage + "</div>";
    }
    group.find('.torrent_info_row_results2').html(results);
}

function _isChecked(group) {
    return group.prev().children().find("a[title='Check torrent']").length === 0;
}

function _isHDSourced(shortInfo) {
     return _contains(shortInfo, "Blu-ray") || _contains(shortInfo, "HD-DVD") || _contains(shortInfo, "HDTV");
}

function _isSD(group) {
    return group.prevAll().find('.basic-movie-list__torrent-edition__sub:contains("High Definition")').length === 0;
}

function _contains(source, keywords) {
    return source.indexOf(keywords) !== -1;
}

function _isExact(source, keywords) {
    var array = source.split(" / ");
    return array.indexOf(keywords) > -1;
}

function _doesntContain(source, keywords) {
    return source.indexOf(keywords) === -1;
}