// ==UserScript==
// @name        PTP Torrent Checker
// @author      Voltaire w/ Updates by Korrug
// @description Assist with checking torrents
// @namespace   PTPTorrentChecker
// @include     https://passthepopcorn.me/torrents.php?id=*
// @version     3.989
// @downloadURL https://gitlab.com/korrug/PTP/raw/master/torrentChecker.user.js
// @updateURL   https://gitlab.com/korrug/PTP/raw/master/torrentChecker.user.js
// @grant       none
// ==/UserScript==

var version = "3.989";

var readyStateCheckInterval = setInterval(() => {
    if (document.readyState === "interactive" || document.readyState === "complete") {
        clearInterval(readyStateCheckInterval);
        main();
    }
}, 100);

function main() {
    // Handle the initially opened torrent if this is a torrent permalink.
    var searchParams = new URLSearchParams( window.document.URL );
    var initiallyOpenedTorrentId = searchParams.get( "torrentid" );
    if ( initiallyOpenedTorrentId !== null ) {
        var group = jQuery( "#torrent_" + initiallyOpenedTorrentId );
        if ( group.length > 0 ) {
            validateGroup( group );
        }
    }

    // Mediainfo is loaded async, so use a mutation observer to detect when it's ready
    var observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        if (mutation.removedNodes[0].textContent === "Loading...") {
            validateGroup(jQuery("#torrent_" + mutation.target.id.split('_').pop()));
        }
      });
    });

    // Validate new groups when opened and ready
    jQuery('tr.torrent_info_row[style*="display: none"], tr.torrent_info_row:hidden').each((key, group) => {
        observer.observe(jQuery("#nfo_text_" + group.id.split('_').pop())[0], {childList: true});
    });
}

/**
 * Runs the suit of tests against a group.
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return null
 */
function validateGroup(group) {
    setup(group);

    // Run generic checks
    checkMediainfoAR(group);
    checkFrameRate(group);
    checkImages(group);
    checkNonconformAnamorphic(group);

    // Run specific checks
    checkBD(group);
    checkRemux(group);
    checkSD(group);
    checkSubtitles(group);
	checkEnglishSubsReq(group);
    checkVariableFPS(group);
    checkHDR10(group);
    checkXcodeAud(group);
    checkHEVC(group);
    checkCodec(group);
    checkSDHDres(group);
    checkAudio(group);
    checkCommentary(group);
    checkVision(group);

    // Run after checks are complete;
    jQuery(window).on("load", () => {
        displayErrors(group, true);
        sendStats(group);
    });
}

/**
 * Runs preflight setup
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return null
 */

function setup(group) {
    // Create someplace centralized to store errors for the group
    group.data("errors", []);

    // Create place to display the results of the script
    group.find('.movie-page__torrent__panel').find('.bbcode-table-guard').last().prepend("<div class='torrent_info_row_results'></div>");
}

/**
 * Runs tests on images
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return null
 */
function checkImages(group) {
    var res = getResolution(group);
    _getScreenshots(group).each((key, image) => {
        // If the image is already loaded
        if (image.complete) {
            validateImage(group, image, res);
        } else {
            jQuery(image).on('load', function() {
                validateImage(group, image, res);
            });
        }
    });
}

/**
 * Compare the torrent's AR with IMDB and add a warning if there is a mismatch
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return null
 */
function checkMediainfoAR(group) {
    var movieAR = jQuery('#movieinfo').find('strong:contains("AR")');

    // If there's no IMDB info then don't worry about it
    if (movieAR.length === 0) {
        return;
    }

    movieAR = movieAR[0].nextSibling.nodeValue.trim();
    var torrentAR = _getTorrentAspectRatio(group);
    var movieARDecimal = movieAR.split(":")[0] / movieAR.split(":")[1];
    var torrentARDecimal = torrentAR.split(":")[0] / torrentAR.split(":")[1];

    // Ignore mismatches that have less than a 6% margin of error
	// Also ignore mismatches for BD source and remuxes
	var shortInfo = getShortInfoText(group);
	if ((movieAR !== torrentAR && (Math.abs(torrentARDecimal-movieARDecimal)/torrentARDecimal > 0.06)) && ((_contains(shortInfo, 'BD25') || _contains(shortInfo, 'BD50') || _contains(shortInfo, 'BD66') || _contains(shortInfo, 'BD100') || _contains(shortInfo, 'Remux') || _contains(shortInfo, 'DVD5') || _contains(shortInfo, 'DVD9')))){
		addError(group, "AR Mismatch is expected but verify OAR.");
	} else if (movieAR !== torrentAR && (Math.abs(torrentARDecimal-movieARDecimal)/torrentARDecimal > 0.06)) {
        addError(group, "AR mismatch between torrent (" + torrentAR + ") and IMDB (" + movieAR + ").");
    }
}

/**
 * Check for improper framerates
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return null
 */
function checkFrameRate(group) {
    var framerate = getMediaInfo(group).find('table.mediainfo__section').find('td:contains("Frame rate:")').next().text().substr(0, 6).replace(',', '.');
    // Force the string to always have 0 decimals, so 25 becomes 25.000 etc
    framerate = parseFloat(framerate).toFixed(3);
    var shortInfo = getShortInfoText(group);
    var year = parseInt(jQuery('.page__title')[0].firstChild.textContent.substr(-9, 4));

    if (_doesntContain(["23.976", "24.000", "25.000", "29.970"], framerate)) {
        addError(group, "Improper framerate.");
    } else if (framerate === "25.000" && _contains(shortInfo, "DVD / NTSC")) {
        addError(group, "Framerate is 25.000fps but this is NTSC.");
    } else if (framerate !== "25.000" && _contains(shortInfo, "DVD / PAL")) {
        addError(group, "Source is PAL but the framerate is not 25.000fps.");
    } else if (framerate === "24.000" ) {
        if(_doesntContain(shortInfo, "Blu-ray") && _doesntContain(shortInfo, "WEB")){
            addError(group, "Framerate is 24.000fps but the source is not Blu-ray or WEB.");
        }else{return;}
    } else if (framerate === "29.970" && year >= 1980 && _doesntContain(shortInfo, "VHS") &&
        _doesntContain(shortInfo, "DVD5") && _doesntContain(shortInfo, "DVD9")) {
        var isNotDocumentary = _doesntContain(jQuery(".panel__heading__title:contains('IMDb tags')").parent().next().text(), "documentary");
        var isNotConcert = _doesntContain(jQuery('.basic-movie-list__torrent-edition__main').text(), "Live Performance");

        if (isNotConcert && isNotDocumentary) {
            addError(group, "Framerate is 29.970.  Unless this was shot on video then it needs to be reverse telecined.");
        }else{return;}
    }else{return;}
}

/**
 * Make sure only correct groups are anamorphic
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return null
 */
function checkNonconformAnamorphic(group){
    var shortInfo = getShortInfoText(group);
    var isAnamorphic = _isAnamorphic(getResolution(group));
	// added a check for 2160p blurays
	var is2160p = _contains(shortInfo, '2160p') && _contains(shortInfo, 'Blu-Ray');
    var is720p1080orBluray = _contains(shortInfo, '720p') || _contains(shortInfo, '1080p') || _contains(shortInfo, 'Blu-ray') || _isExact(shortInfo, 'HDTV');
    var is480p576p = _contains(shortInfo, '480p') || _contains(shortInfo, '576p');
	if (isAnamorphic && is2160p) {
		addError(group, "Non conform resolution: UHD sourced encodes should not be anamorphic.");
	} else if (isAnamorphic && is720p1080orBluray) {
        addError(group, "Non conform resolution: HD sourced encodes should not be anamorphic.");
    } else if (!isAnamorphic && _isExact(shortInfo, 'DVD') && _contains(shortInfo, '264')) {
        addError(group, "Non conform resolution: DVD sourced x264 should be anamorphic.");
    } else if (!isAnamorphic && _isExact(shortInfo, 'TV') && _contains(shortInfo, '264')) {
        addError(group, "Non conform resolution: TV sourced x264 should be anamorphic.");
    } else if (!isAnamorphic && _contains(shortInfo, 'VHS') && _contains(shortInfo, '264')) {
        addError(group, "Non conform resolution: VHS sourced x264 should be anamorphic.");
    } else if (isAnamorphic && _contains(shortInfo, 'AVI')) {
        addError(group, "Non conform resolution: AVIs should not be anamorphic.");
    } else if (_isSD(group) && _contains(shortInfo, 'WEB')){
        if(!is480p576p && !isAnamorphic){
            addError(group, "Non conform resolution: WEB encodes should be either anamorphic or 480/576p depending on source.");
        }
    }
}

function checkBD(group) {
    var shortInfo = getShortInfoText(group);
    // If it's not a BD then we're done here
    if (_doesntContain(shortInfo, 'BD25') && _doesntContain(shortInfo, 'BD50') && _doesntContain(shortInfo, 'BD66') && _doesntContain(shortInfo, 'BD100')) {
        return;
    }
    var mediaInfoText = group.find('blockquote.spoiler').text();
    if (_doesntContain(mediaInfoText, "Disc Title") && _doesntContain(mediaInfoText, "Disc Size")) {
        addError(group, "Missing BDInfo.");
    }
}

/**
 * Remux specific checks
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return null
 */
function checkRemux(group) {
    var shortInfo = getShortInfoText(group);
    // If it's not a remux then we're done here
    if (_doesntContain(shortInfo, 'Remux')) {
        return;
    }
	// Check and see if the remux is UHD or HD
	var res = getResolution(group);
    // could probably remove the shortinfo and base the check on feature film location
	if (_contains(shortInfo, "265")){
		if((res[0] !== 3840) || (res[1] !== 2160)){
			addError(group, "UHD Remuxes must be 2160p.");
		}
	}else{
		if((res[0] !== 1920) || (res[1] !== 1080)){
			addError(group, "HD Remuxes must be 1080p.");
		}
	}
}

function checkSD(group) {
    var shortInfo = getShortInfoText(group);
    var res = getResolution(group);
    var isAnamorphic = _isAnamorphic(res);
    if (_isSD(group) && _isHDSourced(shortInfo) && _contains(shortInfo, '264')) {
    //if (group.prevAll().find('.basic-movie-list__torrent-edition__sub:contains("High Definition")').length === 0 && _isHDSourced(shortInfo)) {
        if (_doesntContain(shortInfo, '480p') && _doesntContain(shortInfo, '576p')) {
            addError(group, "HD sourced SD encodes must be either 480p or 576p.");
        }
    }

    if (_contains(shortInfo, 'AVI') && ((!isAnamorphic && (res[0] > 720 || res[1] > 576)) ||
        (isAnamorphic && (res[2] > 720 || res[3] > 576)))
    ) {
        addError(group, "AVIs cannot be larger than 720x576 (it breaks compatability).");
    }

    // For dvds ensure standard matches between shortinfo and mediainfo
    if (_contains(shortInfo, 'DVD5') || _contains(shortInfo, 'DVD9')) {
        var mediaInfoText = group.find('blockquote.spoiler').text();
        if ((_contains(shortInfo, 'PAL') && _contains(mediaInfoText, ' : NTSC')) ||
            (_contains(shortInfo, 'NTSC') && _contains(mediaInfoText, ' : PAL'))
        ) {
            addError(group, "NTSC / PAL missmatch between shortinfo and mediainfo.");
        }

        if (_doesntContain(mediaInfoText, ' : NTSC') && _doesntContain(mediaInfoText, ' : PAL')) {
            addError(group, "Mediainfo does not list either NTSC or PAL.");
        }
    }
}

function checkSubtitles(group) {
    var groupID = group[0].id.substring(8);
    var shortInfo = getShortInfoText(group);
    var mediaInfoText = group.find('blockquote.spoiler').text();
    var subtitlesInManager = group.find('#subtitle_manager').children('img[alt!="No Subtitles"]').length;
    var subtitlesInMediainfo;
    var subtitleFormats = ['.srt', '.SRT', '.sub', '.SUB', '.ssa', '.SSA', '.sup', '.SUP', '.ass', '.ASS'];
    var subtitleRegex = new RegExp(subtitleFormats.join('$|') + '$');
    var subtitlesInFilelist = group.find('#files_' + groupID).find('td').filter(function() {
        return subtitleRegex.test(jQuery(this).text());
    }).length;

    // Parse the subtitles from the bdinfo or the mediainfo
    if (_contains(shortInfo, 'BD25') || _contains(shortInfo, 'BD50') || _contains(shortInfo, 'BD66') || _contains(shortInfo, 'BD100')) {
        // There's two types of bdinfo, and they list subs differently
        var prefix;
        if (_contains(mediaInfoText, 'SUBTITLES')) {
            prefix = 'Presentation Graphics';
            subtitleRegex = new RegExp(prefix + " +(\\w+)","m");
        } else {
            prefix = "Subtitle: ";
            subtitleRegex = new RegExp(prefix + "(\\w+)","m");
        }

        subtitlesInMediainfo = mediaInfoText.split("\n").filter(text => {
            return _contains(text, prefix);
        }).map(sub => sub.match(subtitleRegex)[1]);

        subtitlesInMediainfo = subtitlesInMediainfo.filter(function(item, pos, self) {
            return self.indexOf(item) == pos;
        }).length;

        if (_contains(mediaInfoText, "Disc Title") && _contains(mediaInfoText, "Disc Size") &&
            (subtitlesInManager !== (subtitlesInMediainfo + subtitlesInFilelist))) {
            addError(group, subtitlesInManager + " internal subs in manager, " + subtitlesInMediainfo + " detected in bdinfo, and " + subtitlesInFilelist + " in file list.");
        }
    } else if (_contains(shortInfo, 'DVD5') || _contains(shortInfo, 'DVD9')){
        subtitlesInMediainfo = mediaInfoText.split(/\n\n/).filter(text => {
            return text.substr(0, 4) === "Text";
        }).map(sub => {
            // Every once in a while a sub won't list a language
            if (sub.match(/Language\s*?: (\w+)/m)) {
                return sub.match(/Language\s*?: (\w+)/m)[1];
            }
        });

        subtitlesInMediainfo = subtitlesInMediainfo.filter(function(item, pos, self) {
            return self.indexOf(item) == pos;
        }).length;

        if (subtitlesInManager !== (subtitlesInMediainfo + subtitlesInFilelist)) {
            addError(group, subtitlesInManager + " internal subs in manager, " + subtitlesInMediainfo + " detected in mediainfo, and " + subtitlesInFilelist + " in file list.");
        }
    }else {
        subtitlesInMediainfo = mediaInfoText.split(/\n\n/).filter(text => {
            return text.substr(0, 4) === "Text";
        }).map(sub => {
            // Every once in a while a sub won't list a language
            if (sub.match(/Language\s*?: (\w+)/m)) {
                //below seems to prevent detecting duplicates, such as English sdh and english subtitle
                return sub.match(/Language\s*?: (\w+)/m)[1];
                //return sub.match(/Language\s*?: (\w+)/m);
            }
            /*if(sub.match(/ID\s*?: (\w+)/m)){
                return sub.match(/ID\s*?: (\w+)/m)[1];
            }*/
        });

        subtitlesInMediainfo = subtitlesInMediainfo.filter(function(item, pos, self) {
            return self.indexOf(item) == pos;
        }).length;

        if (subtitlesInManager !== (subtitlesInMediainfo + subtitlesInFilelist)) {
            addError(group, subtitlesInManager + " internal subs in manager, " + subtitlesInMediainfo + " detected in mediainfo, and " + subtitlesInFilelist + " in file list.");
        }
    }
}

function checkEnglishSubsReq(group) {
	var subtitlesInManager2 = group.find('#subtitle_manager').children('img[alt="English"],img[alt="English - Forced"]').length;
	var movieLang = jQuery('#movieinfo').find('strong:contains("Language")');
    movieLang = movieLang.parent().text().substring(10);
	if(movieLang === "" || movieLang.includes("English")){
        return;
	}else if(!(subtitlesInManager2)){
		addError(group, "No English subtitles detected, verify and then report as trumpable for no English Subs.");
    }else{
        return;
    }
}

function checkVariableFPS(group) {
    //this works mostly, but let's try another way
    var mediaInfoText = group.find('blockquote.spoiler').text();
    //this is the test
    var shortInfo = getShortInfoText(group);
    // If it's a BD, DVD or remux then we're done here
    if (_contains(shortInfo, 'BD25') || _contains(shortInfo, 'BD50') || _contains(shortInfo, 'BD66') || _contains(shortInfo, 'BD100') || _contains(shortInfo, 'DVD5') || _contains(shortInfo, 'DVD9')) {
        return;
    }
    if(_contains(mediaInfoText, 'Minimum frame rate')){
        addError(group, "This encode has variable framerate (with minimum), trumpable for improper FPS.");
    }else{
        return;
    }
}

function checkHDR10(group){
    var shortInfo = getShortInfoText(group);
    var mediaInfoText = group.find('blockquote.spoiler').text();
	var tenbit;
	var hdr;
    var nohdr;
    //console.log(mediaInfoText);
    //first let's set some variables
    if(_contains(mediaInfoText, '10\ bits')){
		tenbit = true;}
    if(_contains(mediaInfoText, 'Mastering\ display\ luminance') || _contains(mediaInfoText, '/ hdr /') || _contains(mediaInfoText, 'Transfer characteristics                 : PQ') || _contains(mediaInfoText, 'Transfer characteristics    : PQ')){
		hdr = true;}
    if(_contains(shortInfo, 'BD25') || _contains(shortInfo, 'BD50') || _contains(shortInfo, 'BD66') || _contains(shortInfo, 'BD100')){
        if(_isExact(mediaInfoText, 'HDR') || _isExact(mediaInfoText, 'HDR10')){
            hdr = true;
        }
    }else if(_contains(mediaInfoText, 'no-hdr')){
        hdr = false;
        nohdr = true;
    }
    //check for HDR on UHD BD sourced encode
    if(!hdr){
        if(_contains(shortInfo, "Blu-ray") && _contains(shortInfo, "2160p") && _doesntContain(shortInfo, "Remux") && _doesntContain(shortInfo, "BD")){
            addError(group, "No HDR metadata detected. Check if the UHD BD/Remux has HDR.");
        }
    }
	//check for an HDR 10-bit film
	if(hdr && tenbit){
        if(!(_isExact(shortInfo, 'HDR10'))){
			addError(group, "ShortInfo must contain HDR10");
		}else if(_isExact(shortInfo, 'HDR10') && _isExact(shortInfo, '10-bit')){
            addError(group, "HDR10 negates the need to mark as 10-bit");
        }else{
            return;
        }
    }else if(_isExact(shortInfo, 'HDR10')){
        if(!hdr){
            if(nohdr){
                addError(group, "Encode settings include no-hdr, this is not HDR.");
            }else{
                addError(group, "Shortinfo indicates HDR10, but MI doesn't indicate HDR");
            }
        }else if(!tenbit){
            addError(group, "Shortinfo indicates HDR10, but MI doesn't indicate 10-bit");
        }else{
            return;
        }
	//check for an hdr film that isn't 10bit
    }else if(hdr && !tenbit){
		if(_doesntContain(shortInfo, 'HDR')){
			addError(group, "ShortInfo must contain HDR");
		}else if(_contains(shortInfo, '10-bit') || _contains(shortInfo, 'HDR10')){
			addError(group, "Mediainfo doesn't reflect 10-bit, but shortinfo indicates, verify.");
		}else{
			return;
		}
	//check for a 10bit film that isn't HDR
    }else if(tenbit && !hdr){
		if(_doesntContain(shortInfo, '10-bit')){
			addError(group, "Shortinfo must include 10-bit in edition field.");
		}else if(_contains(shortInfo, 'HDR') || _contains(shortInfo, 'HDR10')){
            if(nohdr){
                addError(group, "Encode settings include no-hdr, this is not HDR. Change HDR10 to 10-bit");
            }else{
                addError(group, "Mediainfo does not reflect HDR, but shortinfo indicates, verify.");
            }
		}else{
			return;
		}
	}else{
		return;
	}

}

function checkXcodeAud(group) {
    var shortInfo = getShortInfoText(group);
    //if HD sourced, then skip it
    if(_contains(shortInfo, "Blu-ray") || _contains(shortInfo, "HD-DVD") || _contains(shortInfo, "WEB") || _contains(shortInfo, "AVI")){
        return;
    }
    var AudioInfo;
    AudioInfo = getMediaInfo(group).find('caption.mediainfo__section__caption:contains("Audio")').next().text();
    if(_contains(AudioInfo, 'AAC') || _contains(AudioInfo, 'MP3')){
       addError(group, "Trancoded Audio detected, verify");
    }else{return;}
}

function checkHEVC(group) {
    var shortInfo = getShortInfoText(group);
    if(_contains(shortInfo, "HEVC")){
       addError(group, "Shortinfo should be H.265 or x265, not HEVC, please edit.");
    }
}

function checkCodec(group) {
    var shortInfo = getShortInfoText(group);
    var mediainfo = getMediaInfo(group);
    var micodec = mediainfo.find('table.mediainfo__section').find('td:contains("Codec")').next().text();
    //if its a source disc, skip it
    if (_contains(shortInfo, 'BD25') || _contains(shortInfo, 'BD50') || _contains(shortInfo, 'BD66') || _contains(shortInfo, 'BD100') || _contains(shortInfo, 'DVD5') || _contains(shortInfo, 'DVD9')) {
        return;
    }
    //javascript is shit so im doing this in a... different way
    if(_contains(micodec, "HEVC")){
       if(_contains(shortInfo, "H.265") || _contains(shortInfo, "x265")){
          return;
       }else{
           addError(group, "MI indicates UHD but shortInfo doesn't, verify and correct.");
       }
    }else if(_contains(shortInfo, "H.265") || _contains(shortInfo, "265")){
        if(_contains(micodec, "HEVC") || _contains(micodec, "265") ){
            return;
        }else{
            addError(group, "ShortInfo indicates UHD but MI does not, verify and correct.");
        }
    }else if(_contains(micodec, "h264")){
        if(_contains(shortInfo, "H.264")){
            return;
        }else{
            addError(group, "Mismatch between MI Codec and shortInfo Codec, verify and correct");
        }
    }else if(_contains(micodec, "x264")){
        if(_contains(shortInfo, "x264")){
            return;
        }else{
            addError(group, "Mismatch between MI Codec and shortInfo Codec, verify and correct");
        }
    }else if(_contains(micodec, "VC-1")){
        if(_contains(shortInfo, "VC-1")){
            return;
        }else{
            addError(group, "Mismatch between MI Codec and shortInfo Codec, verify and correct");
        }
    }else{
        return;
    }
}

function checkSDHDres(group) {
    var shortInfo = getShortInfoText(group);
    var res = getResolution(group);
    if (_contains(shortInfo, '264')) {
        if (_contains(shortInfo, '480p')){
            if(res[0] > 854 || res[1] > 480){
                addError(group, "Non-conform resolution. 480p is limited to a maximum of 854x480.");
            }
        }else if(_contains(shortInfo, '576p')){
            if(res[0] > 1024 || res[1] > 576){
                addError(group, "Non-conform resolution. 576p is limited to a maximum of 1024x576.");
            }
        }else if(_contains(shortInfo, '720p')){
            if(res[0] > 1280 || res[1] > 720){
                addError(group, "Non-conform resolution. 720p is limited to a maximum of 1280x720.");
            }
        }else if(_contains(shortInfo, '1080p')){
            if(res[0] > 1920 || res[1] > 1080){
                addError(group, "Non-conform resolution. 1080p is limited to a maximum of 1920x1080.");
            }
        }else if(_contains(shortInfo, '2160p')){
            if(res[0] > 3840 || res[1] > 2160){
                addError(group, "Non-conform resolution. 2160p is limited to a maximum of 3840x2160.");
            }
        }else{
            return;
        }
    }else{
        return;
    }
}

function checkAudio(group) {
    var shortInfo = getShortInfoText(group);
	if(_doesntContain(shortInfo, '2160p')){
		return;
	}
	var mediaInfoText = group.find('blockquote.spoiler').text();
	if(_contains(mediaInfoText, 'Atmos') && _doesntContain(shortInfo, 'Dolby Atmos')){
		addError(group, "shortInfo must contain Dolby Atmos");
	}else if(_contains(shortInfo, 'Dolby Atmos') && _doesntContain(mediaInfoText, 'Atmos')){
		addError(group, "shortInfo indicates Atmos but MI doesn't. Verify");
	}
	if(_contains(mediaInfoText, 'DTS:X') || _contains(mediaInfoText, 'X / MA')){
		if(_doesntContain(shortInfo, 'DTS:X')){
			addError(group, "shortInfo must contain DTS:X");
		}else{
			return;
		}
	}else if(_contains(shortInfo, 'DTS:X')){
		if(_doesntContain(mediaInfoText, 'DTS:X') && _doesntContain(mediaInfoText, 'Format profile : X')){
			addError(group, "shortInfo indicates DTS:X but MI doesn't. Verify");
		}else{
			return;
		}
	}else{
		return;
	}
}

function checkCommentary(group){
	var AudioInfo = getMediaInfo(group).find('caption.mediainfo__section__caption:contains("Audio")').next().text();
	if(_contains(AudioInfo, 'Commentary')){
		var shortInfo = getShortInfoText(group);
		if(_doesntContain(shortInfo, 'With Commentary')){
			addError(group, "shortInfo should contain 'With Commentary'");
		}else{
			return;
		}
	}else{
		return;
	}
}

function checkVision(group){
    var shortInfo = getShortInfoText(group);
    // If it's not a BD then we're done here
    if (_doesntContain(shortInfo, 'BD25') && _doesntContain(shortInfo, 'BD50') && _doesntContain(shortInfo, 'BD66') && _doesntContain(shortInfo, 'BD100')) {
        return;
    }
    var mediaInfoText = group.find('blockquote.spoiler').text();
    if(_contains(mediaInfoText, 'Dolby Vision')){
        if(_doesntContain(shortInfo, 'Dolby Vision')){
            addError(group, "shortInfo must contain 'Dolby Vision'");
        }
    }else if(_contains(shortInfo, 'Dolby Vision')){
        if(_doesntContain(mediaInfoText, 'Dolby Vision')){
            addError(group, "shortInfo indicates Dolby Vision, but MI doesn't. Verify");
        }
    }else{
        return;
    }
}

/**
 * Put a red border around any images that are the incorrect size
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @param {object} image - The html image element to run the analysis on.
 * @param {array} resolution - [DARWidth, DARHeight, SARWidth, SARHeight]
 * @return null
 */
function validateImage(group, image, resolution) {
    // What is the acceptable margin of error allowed in the image's size?
    var DARWidth = resolution[0];
    var DARHeight = resolution[1];
    var SARWidth = resolution[2];
    var SARHeight = resolution[3];
    var imageWidth = image.naturalWidth;
    var imageHeight = image.naturalHeight;
    var isAnamorphic = _isAnamorphic(resolution);
    var widthErrorMargin = isAnamorphic ? 20 : 3;
    var heightErrorMargin = isAnamorphic ? 10 : 2;

    // Is the imagine with the allowed margin of error?
    if (_dimensionsWithinMarginOfError(DARWidth, imageWidth, DARHeight, imageHeight, widthErrorMargin, heightErrorMargin)) {
        return;
    }

    // No?  Did the server calculate the DAR wrong?  Let's check the alternative way.
    var torrentAR = _getTorrentAspectRatio(group).split(':');

    if (isAnamorphic && DARWidth === SARWidth) {
        DARWidth = (SARHeight / torrentAR[1] * torrentAR[0]);
        DARHeight = SARHeight;
        if (_dimensionsWithinMarginOfError(DARWidth, imageWidth, DARHeight, imageHeight, widthErrorMargin, heightErrorMargin)) {
            return;
        }
    } else if (isAnamorphic && DARHeight === SARHeight) {
        DARWidth = SARWidth;
        DARHeight = (SARWidth / torrentAR[0] * torrentAR[1]);
        if (_dimensionsWithinMarginOfError(DARWidth, imageWidth, DARHeight, imageHeight, widthErrorMargin, heightErrorMargin)) {
            return;
        }
    }

    // No?  Well I guess it's wrong then.  Let's throw a nice obnoxious red border around it.
    addError(group, "One, or more, images are the wrong size.");
    jQuery(image).css({'border': "5px solid red"});
}

/**
 * @param  {int} width - Expected width
 * @param  {int} imageWidth - Width of the image
 * @param  {int} height - Expected height
 * @param  {int} imageHeight - Height of the image
 * @param  {int} widthErrorMargin - Acceptable margin of error for the width in pixels
 * @param  {int} heightErrorMargin - Acceptable margin of error for the height in pixels
 * @return {boolean} Are the dimensions withing the acceptable margin of error?
 */
function _dimensionsWithinMarginOfError(width, imageWidth, height, imageHeight, widthErrorMargin, heightErrorMargin) {
    return Math.abs(width-imageWidth) <= widthErrorMargin && Math.abs(height-imageHeight) <= heightErrorMargin;
}

/**
 * Get a single mediainfo to work with.  Is there just one? Cool, use that.
 * Are there multiple ones? Choose the first VOB. Is there no VOB?
 * They're probably all mkv/avi, just take the first one.
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return {mediainfo} single jQuery object holding the correct mediainfo
 */
function getMediaInfo(group) {
    var mediainfos = group.find('table.mediainfo--in-release-description');
    if (mediainfos.length === 1) {
        return mediainfos;
    } else {
        var dvdMediaInfo = mediainfos.prevAll('a:contains("VOB")').first().next();
        if (dvdMediaInfo.length === 1) {
            return dvdMediaInfo;
        } else {
            return mediainfos.first();
        }
    }

}

/**
 * Get the expected dimensions of images for a group
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return {Array} [DARWidth, DARWeight, SARWidth, SARHeight]
 */
function getResolution(group) {
    var mediainfo = getMediaInfo(group);
    // Get the listed resolution in the format ["DARWidthxDARWeight", "SARWidthxSARHeight"]
    var res = mediainfo.find('table.mediainfo__section').find('td:contains("Resolution")').next().text().split(" ~> ");

    // Sometimes the parser will display 2160p instead of a ### x ### resolution
    if (res[0] === '2160p') {
        return [3840, 2160, false, false];
    }

    // Sometimes the parser will display 1080p or 1080i instead of a ### x ### resolution
    if (res[0] === '1080p' || res[0] === '1080i') {
        return [1920, 1080, false, false];
    }

    // Sometimes the parser will display 720p instead of a ### x ### resolution
    if (res[0] === '720p') {
        return [1280, 720, false, false];
    }

    // If the resolution is anamorphic then return both DAR and SAR
    if (res[1]) {
        return _splitAndParseResolution(res[1]).concat(_splitAndParseResolution(res[0]));
    }

    // Otherwise just return the DAR and set the SAR values to false
    return _splitAndParseResolution(res[0]).concat([false, false]);
}

function _splitAndParseResolution(resString) {
    return [parseInt(resString.split('x')[0]), parseInt(resString.split('x')[1])];
}

/**
 * Get the "short info" description for a torrent
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @return {String} e.g. "DVD9 / VOB IFO / DVD / PAL"
 */
function getShortInfoText(group) {
    return group.prev().find('.torrent-info-link').text();
}

/**
 * Add an error to the error array and update the visible error list
 * @param {object} group - The jquery "group" element to run the analysis on.
 */
function addError(group, newError) {
    if (newError && _doesntContain(group.data("errors"), newError)) {
        group.data("errors").push(newError);
        displayErrors(group);
    }
}

/**
 * Display all current errors
 * @param {object} group - The jquery "group" element to run the analysis on.
 * @param {boolean} finalRun - Set to true when it's the last run
 */
function displayErrors(group, finalRun) {
    var results = '';

    if (group.data("errors").length > 0) {
        var errorMessage = "WARNING(S):";
        group.data("errors").forEach(value => {errorMessage += "<br />&#8227; " + value;});

        results = "<div class='torrent_info_row_errors' style='color: red; font-size: 1.5em; margin-bottom: 1em;'>" +
            errorMessage + "</div>";
    }

    if (group.data("errors").length === 0 && !_isChecked(group) && finalRun) {
        results = "<div class='torrent_info_row_errors' style='color: green; font-size: 1em; margin-bottom: 1em;'>" +
            "All basic torrent checks passed, but you should still review it manually.</div>";
    }else if(group.data("errors").length === 0 && _isChecked(group) && finalRun) {
        results = "<div class='torrent_info_row_errors' style='color: green; font-size: 1em; margin-bottom: 1em;'>" +
            "All basic torrent checks passed, torrent is already pre-checked.</div>";
    }

    group.find('.torrent_info_row_results').html(results);
}

/**
 * Send error stats to the server to we can collect metrics
 * @param {object} group - The jquery "group" element to run the analysis on.
 */
function sendStats(group) {
    var stats = {
        ScriptVersion: version,
        GroupID: group[0].id.substring(8),
        TorrentID: TGroupID, //Populated on the torrent page automagically
        Checked: _isChecked(group),
        Mediainfos: group.find('table.mediainfo--in-release-description').length,
        Screenshots: _getScreenshots(group).length,
        Errors: group.data("errors")
    };

    jQuery.post("tools.php?action=ajax_checkingstats", AddAntiCsrfTokenToPostData(stats));
}

function _isAnamorphic(res) {
    return res[2] !== false;
}

function _isChecked(group) {
    return group.prev().children().find("a[title='Check torrent']").length === 0;
}

function _isHDSourced(shortInfo) {
     return _contains(shortInfo, "Blu-ray") || _contains(shortInfo, "HD-DVD") || _contains(shortInfo, "HDTV");
}

function _isSD(group) {
    //return group.nextAll().find('.basic-movie-list__torrent-edition__sub:contains("High Definition")').length === 0;
    return group.prevAll().find('.basic-movie-list__torrent-edition__sub:contains("High Definition")').length === 0;
}

function _getScreenshots(group) {
    return group.find('img.bbcode__image:not(.hidden a img.bbcode__image)');
}

function _getTorrentAspectRatio(group) {
    return getMediaInfo(group).find('table.mediainfo__section').find('td:contains("Aspect ratio:")').next().text();
}

function _contains(source, keywords) {
    return source.indexOf(keywords) !== -1;
}

function _isExact(source, keywords) {
    var array = source.split(" / ");
    return array.indexOf(keywords) > -1;
}

function _doesntContain(source, keywords) {
    return source.indexOf(keywords) === -1;
}